create table artists
(
    id             varchar(255) not null
        primary key,
    name           varchar(255) null,
    picture        varchar(255) null,
    picture_small  varchar(255) null,
    picture_medium varchar(255) null,
    picture_big    varchar(255) null
);

create table albums
(
    id           varchar(255) not null
        primary key,
    title        varchar(255) null,
    cover        varchar(255) null,
    cover_small  varchar(255) null,
    cover_medium varchar(255) null,
    cover_big    varchar(255) null,
    artist_id    varchar(255) null,
    constraint albums_artists_id_fk
        foreign key (artist_id) references artists (id)
);

create table genres
(
    id   int auto_increment
        primary key,
    name varchar(255) null
);

create table album_genres
(
    album_id varchar(255) not null,
    genre_id int          not null,
    primary key (album_id, genre_id),
    constraint FKd975yib1w5u2k4dpkjmswfu1y
        foreign key (album_id) references albums (id),
    constraint FKnc7pifo8642ai7gtk672bekkc
        foreign key (genre_id) references genres (id)
);

create table tracks
(
    track_id  varchar(255) not null
        primary key,
    title     varchar(255) null,
    duration  int          null,
    `rank`    int          null,
    preview   varchar(255) null,
    artist_id varchar(255) null,
    album_id  varchar(255) null,
    genre_id  int          null,
    constraint tracks_albums_id_fk
        foreign key (album_id) references albums (id),
    constraint tracks_artists_id_fk
        foreign key (artist_id) references artists (id),
    constraint tracks_genres_id_fk
        foreign key (genre_id) references genres (id)
);

create table users
(
    username varchar(60) default '' not null
        primary key,
    password varchar(255)           null,
    enabled  tinyint(1)             null
)
    collate = utf8mb4_unicode_ci;

create table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    constraint username_authority
        unique (username, authority),
    constraint FK__users
        foreign key (username) references users (username)
)
    collate = utf8mb4_unicode_ci;

create table users_details
(
    id            int auto_increment
        primary key,
    email         varchar(50)                      not null,
    first_name    varchar(50)                      null,
    last_name     varchar(50)                      null,
    age           int                              null,
    gender        enum ('male', 'female', 'other') null,
    picture       longblob                         null,
    user_username varchar(60)                      null,
    date_created  varchar(255)                     null,
    description   varchar(255)                     null,
    constraint users_email_uindex
        unique (email),
    constraint users_details_users_username_fk
        foreign key (user_username) references users (username)
)
    collate = utf8mb4_unicode_ci;

create table playlists
(
    playlist_id        int auto_increment
        primary key,
    title              varchar(255) null,
    duration           int          null,
    `rank`             int          null,
    cover              varchar(255) null,
    enabled            tinyint(1)   null,
    user_id            int          null,
    date_created       varchar(255) null,
    is_artist_repeated tinyint(1)   null,
    picture            varchar(255) null,
    top_tracks_enabled tinyint(1)   null,
    constraint playlists_users_details_id_fk
        foreign key (user_id) references users_details (id)
);

create table playlist_genres
(
    playlist_id int null,
    genre_id    int null,
    constraint playlist_genres_genres_id_fk
        foreign key (genre_id) references genres (id),
    constraint playlist_genres_playlists_playlist_id_fk
        foreign key (playlist_id) references playlists (playlist_id)
);

create table playlists_tracks
(
    id          int auto_increment
        primary key,
    playlist_id int          null,
    track_id    varchar(255) null,
    constraint playlists_tracks_playlists_playlist_id_fk
        foreign key (playlist_id) references playlists (playlist_id),
    constraint playlists_tracks_tracks_track_id_fk
        foreign key (track_id) references tracks (track_id)
);

