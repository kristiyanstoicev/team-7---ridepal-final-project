var adonisPlayer = {},
    adonisAllPlaylists = [],
    adonisPlayerID = 'adonis_jplayer_main',
    adonisPlayerContainer = 'adonis_jp_container',
    adonisPlaylist,
    homePlaylist = [
        {
            title: "INTRO",
            artist: "Gunnar Olsen{#link1}",
            mp3: "https://cdns-preview-3.dzcdn.net/stream/c-3c40a4b958f5d7fb431718ec00e5be81-1.mp3",
            poster: "https://images.unsplash.com/flagged/photo-1569521739482-5443615d3725?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
        }
    ],
    currentPlaylistId;

let preloaderPlayHtml = '<span class="shadow"></span><div class="icons"><span class="animate-loading"><span class="adonis-icon icon-5x"><svg xmlns="http://www.w3.org/2000/svg" version="1.1"><use xlink:href="#icon-brand-play-gradient"></use></svg></span></span><span class="adonis-icon icon-5x"><svg xmlns="http://www.w3.org/2000/svg" version="1.1"><use xlink:href="#icon-brand-play"></use></svg></span></div>';


jQuery(document).ready(function ($) {
    "use strict";

    adonisPlayer.init = function () {
        adonisPlaylist = new adonisJPlayerPlaylist({
                jPlayer: '#' + adonisPlayerID,
                cssSelectorAncestor: "#" + adonisPlayerContainer
            }, homePlaylist,
            {
                playlistOptions: {
                    enableRemoveControls: true
                },
                swfPath: "/assets/js",
                supplied: "oga, mp3",
                useStateClassSkin: true,
                autoBlur: false,
                smoothPlayBar: false,
                keyEnabled: false,
                audioFullScreen: true,
                display: false,
                autoPlay: true,
            });

        // player loaded event
        $("#" + adonisPlayerID).bind($.jPlayer.event.loadeddata, function (event) {
            var Artist = adonisExtractArtistLink($(this).data("jPlayer").status.media.artist),
                Poster = $(this).data("jPlayer").status.media.poster,
                Title = $(this).data("jPlayer").status.media.title;
            $('#' + adonisPlayerContainer + ' .current-item .song-poster img').attr('src', Poster);
            $("#" + adonisPlayerID).find('img').attr('alt', '');
        });

        $(document).on('click', '#adonis-playlist .playlist-item .song-poster', function () {
            $(this).parent().find('.jp-playlist-item').trigger('click');
        });

        /**
         * event play
         */
        $("#" + adonisPlayerID).bind($.jPlayer.event.play + ".jp-repeat", function (event) {

            // poster
            var poster = $(this).data("jPlayer").status.media.poster;
            $('#' + adonisPlayerContainer).find('.adonis-player .song-poster img').attr('src', poster);

            // blurred background
            $('#' + adonisPlayerContainer).find('.blurred-bg').css('background-image', 'url(' + poster + ')');


            // astist
            var artist = adonisExtractArtistLink($(this).data("jPlayer").status.media.artist);
            if (artist.name) {
                $('#' + adonisPlayerContainer + ' .artist-name').html('<a href="' + artist.link + '">' + artist.name + '</a>');
            } else {
                $('#' + adonisPlayerContainer + ' .artist-name').html(artist.name);
            }

            // activate album
            if (typeof currentPlaylistId !== 'undefined') {
                $("[data-album-id='" + currentPlaylistId + "']").addClass('jp-playing');
            }

        });

        $('.adonis-mute-control').click(function () {
            var muteControl = $(this);

            if (muteControl.hasClass('muted')) {
                var volume = muteControl.attr('data-volume');
                $("#" + adonisPlayerID).jPlayer("unmute");
                muteControl.removeClass('muted');
                $("#" + adonisPlayerID).jPlayer("volume", volume);
            } else {
                var volume = $("#" + adonisPlayerID).data("jPlayer").options.volume;
                muteControl.attr('data-volume', volume);
                $("#" + adonisPlayerID).jPlayer("mute").addClass('muted');
                muteControl.addClass('muted');
            }
        });

        /**
         * event pause
         */
        $("#" + adonisPlayerID).bind($.jPlayer.event.pause + ".jp-repeat", function (event) {
            // deactivate album
            if (typeof currentPlaylistId !== 'undefined') {
                $("[data-album-id='" + currentPlaylistId + "']").removeClass('jp-playing');
            }
        });

        /**
         * extract artist link from artist string
         * @param str e.g. "Artist name{http://artist.com}"
         * @return return object containing two key link and name
         */
        function adonisExtractArtistLink(str) {
            var re = /{(.*?\})/,
                strRe = str.replace(re, ''),
                Match = str.match(re, '')
                , Link;
            if (Match != null) {
                var Link = Match[1].replace('}', '');
            }
            return {link: Link, name: strRe};
        }


        /* Modern Seeking */

        var timeDrag = false; /* Drag status */

        $('.jp-progress').mousedown(function (e) {
            timeDrag = true;
            var percentage = updatePercentage(e.pageX, $(this));
            $(this).addClass('dragActive');

            updatebar(percentage);
        });

        $(document).mouseup(function (e) {
            if (timeDrag) {
                timeDrag = false;
                var percentage = updatePercentage(e.pageX, $('.jp-progress.dragActive'));
                $('.jp-progress.dragActive');
                if (percentage) {
                    $('.jp-progress.dragActive').removeClass('dragActive');
                    updatebar(percentage);
                }
            }
        });

        $(document).mousemove(function (e) {
            if (timeDrag) {
                var percentage = updatePercentage(e.pageX, $('.jp-progress.dragActive'));
                updatebar(percentage);
            }
        });

        //update Progress Bar control
        var updatebar = function (percentage) {

            var maxduration = $("#" + adonisPlayerID).jPlayer.duration; //audio duration

            $('.jp-play-bar').css('width', percentage + '%');

            $("#" + adonisPlayerID).jPlayer("playHead", percentage);
            // Update progress bar and video currenttime

            $("#" + adonisPlayerID).jPlayer.currentTime = maxduration * percentage / 100;

            return false;
        };


        function updatePercentage(x, progressBar) {
            var progress = progressBar;
            var maxduration = $("#" + adonisPlayerID).jPlayer.duration; //audio duration
            var position = x - progress.offset().left; //Click pos
            var percentage = 100 * position / progress.width();
            //Check within range
            if (percentage > 100) {
                percentage = 100;
            }
            if (percentage < 0) {
                percentage = 0;
            }
            return percentage;
        }


        var volumeDrag = false;
        $(document).on('mousedown', '.jp-volume-bar', function (e) {
            volumeDrag = true;
            updateVolume(e.pageX);
        });

        $(document).mouseup(function (e) {
            if (volumeDrag) {
                volumeDrag = false;
                updateVolume(e.pageX);
            }
        });

        $(document).mousemove(function (e) {
            if (volumeDrag) {
                updateVolume(e.pageX);
            }
        });

        //update Progress Bar control
        var updateVolume = function (x) {
            var progress = $('.jp-volume-bar');
            var position = x - progress.offset().left; //Click pos
            var percentage = 100 * position / progress.width();

            //Check within range
            if (percentage > 100) {
                percentage = 100;
            }
            if (percentage < 0) {
                percentage = 0;
            }
            $("#" + adonisPlayerID).jPlayer("volume", (percentage / 100));
        };

        // remove track item
        $(document).on('click', '.remove-track-item-playlist', function () {
            var parentLi = openMenu.parents('li.item');
            adonisPlaylist.remove(parentLi.length - 1);
        });

        $(document).on('click', '.remove-track-item-current', function () {
            adonisPlaylist.remove(adonisPlaylist.current);
        });


        /**
         * Function to add track. add track if id not found and return index. If found it return the index
         * @param track track id
         * @returns index number of the track in the playlist
         */
        adonisPlayer.addTrack = function (track) {
            var _track = tracks[track];
            var foundTrack = false;
            var _return;
            adonisPlaylist.playlist.forEach(function (value, index) {
                if (value.id == track) {
                    foundTrack = true;
                    _return = index;
                }
            });

            if (foundTrack === false) {
                adonisPlaylist.add(_track);
                _return = adonisPlaylist.playlist.length - 1;
            }
            return _return;
        }

        /**
         * function to transfer song poster and play button to a larger view. eg. homepage 3 top album listener
         * @param selector
         */
        adonisPlayer.transferAlbum = function (selector) {
            $(document).on('click', selector, function (e) {
                e.preventDefault();
                var PosterTarget = $(this).attr('data-poster-target'),
                    PosterImage = $(this).attr('data-poster'),
                    track = $(this).attr('data-track');

                var PosterClone = $(PosterTarget).clone();
                PosterClone.css('background-image', 'url(' + PosterImage + ')').fadeOut(0);
                PosterClone.insertAfter($(PosterTarget));

                $(PosterTarget).fadeOut('slow', function () {
                    $(this).remove();
                });
                PosterClone.fadeIn('slow');
                var Index = adonisPlayer.addTrack(track);
                adonisPlaylist.play(Index)
            });
        }
        adonisPlayer.transferAlbum('.transfer-album');

        //adonis album play button
        $(document).on('click', '.adonis-album-button', function (e) {
            var albumId = parseInt($(this).attr('data-album-id'));

            // set play list if not set yet
            if (albumId && typeof adonisAllPlaylists[albumId] !== 'undefined' && currentPlaylistId !== albumId) {
                adonisPlaylist.setPlaylist(adonisAllPlaylists[albumId]);
                currentPlaylistId = albumId;
            }

            // play or pause
            if ($('#' + adonisPlayerID).data().jPlayer.status.paused) {
                setTimeout(function () {
                    adonisPlaylist.play(0);
                }, 700);
            } else {
                adonisPlaylist.pause();
            }

        });

        adonisPlayer.addPlaylist = function (albumId) {
            if (albumId && typeof adonisAllPlaylists[albumId] !== 'undefined') {
                adonisAllPlaylists[albumId].forEach(function (_value) {
                    adonisPlaylist.add(_value);
                });
            }
        }

        // init end
    };

    // adonisAllPlaylists[0] = [
    //     {
    //         title:"Cro Magnon Man",
    //         artist:"The Stark Palace 2{#link2}",
    //         mp3:"https://cdns-preview-3.dzcdn.net/stream/c-3c40a4b958f5d7fb431718ec00e5be81-1.mp3",
    //         poster: "../assets/images/browse/browse-overview-4.jpg"
    //     },
    //     {
    //         title:"Cro Magnon Man 3",
    //         artist:"The Stark Palace 3{#link2}",
    //         mp3:"https://cdns-preview-8.dzcdn.net/stream/c-86fefd9738a2a6e337736f0261570acc-5.mp3",
    //         poster: "../assets/images/browse/browse-overview-4.jpg"
    //     }
    // ];


    $(window).imagesLoaded(function () {
        setTimeout(function () {
            adonisPlayer.init();
        }, 100);

        setTimeout(function () {
            adonisPlaylist.setPlaylist(adonisAllPlaylists[0]);
        }, 5000);
    });

    $(document).on("click", "#playButtonSong", function () {
        let track = [{
            title: $(this).attr("data-song-title"),
            artist: $(this).attr("data-song-artist"),
            mp3: $(this).attr("data-song-preview"),
            poster: $(this).attr("data-song-cover")
        }];
        adonisPlaylist.setPlaylist(track);
    });

    $(document).on("click", "#playButton", function () {
        // alert("play");
        // let id = /\[(.*?)\]/g.exec($(this).prop('class'));
        // alert('playlist id:' + id[1]);
        let id = $(this).attr("data-playlist");

        const testPlaylist = [];
        const ajaxURL = '/playlist/' + id + '/songs';

        $.ajax({
            url: ajaxURL,
            type: 'GET',
            contentType: 'application/json',
            dataType: 'json',
            success: function (result) {
                console.log(result);
                let playlist = [];
                result.tracks.forEach(addTrack);

                function addTrack(item) {
                    let track = {
                        title: item.title,
                        artist: item.artist.name,
                        mp3: item.preview,
                        poster: item.album.cover
                    };
                    playlist.push(track);
                }

                console.log(playlist);
                adonisPlaylist.setPlaylist(playlist);
            },
            error: function () {
                console.log("error from 1st ajax");
            }
        });
    });

    $(document).on("click", ".playlist-title", function (event) {
        event.preventDefault();
        const url = $(this).children().prop('href');
        // let playlistID = /\/([0-9]+)\//g.exec(url); //regex example
        // alert(playlistID[1]); //showing 2nd group
        let playlistID = $(this).attr("data-playlist");
        const ajaxURL = '/playlist/' + playlistID + '/songs';
        $.ajax({
            url: ajaxURL,
            type: 'GET',
            data: JSON.stringify(url),
            contentType: 'application/json',
            dataType: 'json',
            success: function (result) {
                console.log(result);
            },
            error: function () {
                console.log("error from 1st ajax");
            }
        }).done(function () {

            if (!$("#site-content-inner").hasClass('loaded')) {
                if ($('.tab-preloader').length < 1) {
                    $('#site-content-inner').append('<div class="preloader tab-preloader"><div class="preloader-overlay"></div></div>');
                }
                $('.tab-preloader').append('<div class="loader-icon"><div class="tab-loader">' + adonisObj.preloaderPlayHtml + '</div></div>');
            }

            $.ajax({
                url: '/playlist/' + playlistID,
                type: 'GET',
                data: JSON.stringify(url),
                contentType: 'text/html',
                dataType: 'html',
                success: function (result) {
                    $(document).write(result);
                },
                error: function () {
                    alert("error from 2nd ajax")
                }
            }).done(function () {
                alert("2nd ajax done");
            })
        });
    });

    // jquery end
});