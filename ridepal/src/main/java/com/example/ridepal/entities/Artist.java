package com.example.ridepal.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "artists")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Artist {


    @JsonProperty("id")
    @Id
    @Column(name = "id")
    String artist_id;

    @JsonProperty("name")
    @Column(name = "name")
    String name;

    @JsonProperty("picture")
    @Column(name = "picture")
    String picture;

    @JsonProperty("picture_small")
    @Column(name = "picture_small")
    public String picture_small;

    @JsonProperty("picture_medium")
    @Column(name = "picture_medium")
    public String picture_medium;

    @JsonProperty("picture_big")
    @Column(name = "picture_big")
    public String picture_big;
}
