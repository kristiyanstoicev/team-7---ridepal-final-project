package com.example.ridepal.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "users")

public class User {

    @Id
    @Column(name = "username")
    @Email
    private String username;

    @Column(name = "password")
    @Size(min = 1, message = "Password is required")
    private String password;

    @Column(name = "enabled")
    private boolean enabled;
}
