package com.example.ridepal.entities.common;

public enum GenderType {
    male,
    female,
    other;
}
