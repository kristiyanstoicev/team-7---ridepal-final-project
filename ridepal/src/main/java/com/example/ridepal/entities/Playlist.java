package com.example.ridepal.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "playlists")
@Setter
@Getter

@AllArgsConstructor
public class Playlist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "playlist_id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "duration")
    private int duration;

    @Column(name = "rank")
    private int rank;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name= "is_artist_repeated")
    boolean isArtistRepeated;

    @Column(name= "top_tracks_enabled")
    boolean isTopTracksEnabled;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserDetail creator;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "playlists_tracks",
            joinColumns = @JoinColumn(name = "playlist_id"),
            inverseJoinColumns = @JoinColumn(name = "track_id")
    )
    List<Track> tracks;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(
            name = "playlist_genres",
            joinColumns = @JoinColumn(name = "playlist_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id")
    )
    List<Genre> genres;

    @Column(name = "date_created")
    private String dateCreated;


    @Column(name = "picture")
    private String picture;


    public Playlist() {
        this.genres=new ArrayList<>();
        this.tracks=new ArrayList<>();
    }
}
