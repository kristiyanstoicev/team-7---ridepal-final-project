package com.example.ridepal.entities;


import com.example.ridepal.entities.common.GenderType;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


import javax.persistence.*;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;


@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users_details")
public class UserDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;


    @Column(name = "email")
    private String email;

    @Lob
    private String picture;

    @OneToOne
    @JoinColumn(name = "user_username")
    private User user;

    @Column(name = "age")
    private int age;

    @Enumerated(EnumType.STRING)
    private GenderType gender;

    @Column(name = "description")
    private String description;

    @Column(name = "date_created")
    private String dateCreated;
}

