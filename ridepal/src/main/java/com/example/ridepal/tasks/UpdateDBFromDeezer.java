package com.example.ridepal.tasks;

import com.example.ridepal.controllers.REST.deezerAPI.DeezerService;
import com.example.ridepal.controllers.REST.deezerAPI.searchDTOs.Playlist;
import com.example.ridepal.controllers.REST.deezerAPI.searchDTOs.SearchDTO;
import com.example.ridepal.controllers.REST.deezerAPI.tracksDTOs.TrackDTO;
import com.example.ridepal.entities.*;
import com.example.ridepal.repositories.*;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


public final class UpdateDBFromDeezer {
    private static final String DISPLAY_MESSAGE = "Updating DB tracks...";

    private Retrofit retrofit = new Retrofit.Builder().baseUrl("https://api.deezer.com")
            .addConverterFactory(JacksonConverterFactory.create())
            .build();

    private TrackRepository trackRepository;
    private ArtistRepository artistRepository;
    private AlbumRepository albumRepository;
    private DeezerService deezerService = retrofit.create(DeezerService.class);
    private int wantedNumberOfTracks;
    private Genre genre;


    public UpdateDBFromDeezer(GenreRepository genreRepository, TrackRepository trackRepository,
                              ArtistRepository artistRepository, AlbumRepository albumRepository,
                              String genreFilter, int wantedNumberOfTracks) {
        this.trackRepository = trackRepository;
        this.artistRepository = artistRepository;
        this.albumRepository = albumRepository;
        this.wantedNumberOfTracks = wantedNumberOfTracks;
        genre = genreRepository.findByName(genreFilter);

        Set<Track> tracksFromDeezer = getAllTracksInPlaylist(getPlaylistsWithEnoughNumberOfTracks(genreFilter));
        System.out.printf("Tracks with genre: %s found in Deezer: %d with Genre: %s %n", genreFilter, tracksFromDeezer.size(), genre);

        System.out.println(DISPLAY_MESSAGE);
        populateDB(tracksFromDeezer);
    }

    private List<Playlist> getPlaylistsWithEnoughNumberOfTracks(String genre) {
        AtomicInteger numberOfTracks = new AtomicInteger(0);
        List<Playlist> playlists = Collections.synchronizedList(new ArrayList<>());
        List<Playlist> result = Collections.synchronizedList(new ArrayList<>());
        AtomicInteger index = new AtomicInteger(0);
        while (numberOfTracks.get() < wantedNumberOfTracks) {
            Call<SearchDTO> call = deezerService.getPlayListsByGenre(genre, index.get());
            try {
                Response<SearchDTO> response = call.execute();
                assert response.body() != null;
                playlists.addAll(response.body().getData());
                for (Playlist playlist : playlists) {
                    numberOfTracks.getAndAdd(playlist.getNbTracks());
                    result.add(playlist);
                    if (numberOfTracks.get() >= wantedNumberOfTracks) {
                        break;
                    }
                }
                System.out.printf("Number of tracks from %d playlists: %d%n", result.size(), numberOfTracks.get());
                index.getAndAdd(25);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private Set<Track> getAllTracksInPlaylist(List<Playlist> playlists) {
        Set<Track> tracks = new HashSet<>();
        for (Playlist playlist : playlists) {
            Call<TrackDTO> call = deezerService.getAllTracksFromPlaylist(playlist.getId());
            try {
                Response<TrackDTO> response = call.execute();
                assert response.body() != null;
                TrackDTO trackDAO = response.body();
                if (trackDAO.getData() != null) {
                    tracks.addAll(trackDAO.getData());
                }
                AtomicInteger index = new AtomicInteger(25);
                while (trackDAO.getNext() != null) {
                    Call<TrackDTO> nextPageOfPlaylistCall = deezerService.getAllTracksFromPlaylist(playlist.getId(), index.get());
                    Response<TrackDTO> nextPageResponse = nextPageOfPlaylistCall.execute();
                    trackDAO = nextPageResponse.body();
                    assert trackDAO != null;
                    if (trackDAO.getData() != null) {
                        tracks.addAll(trackDAO.getData());
                    }
                    index.getAndAdd(25);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return tracks;
    }

    private void populateDB(Set<Track> tracksFromDeezer) {
        tracksFromDeezer.forEach(track -> {
            Artist savedArtist = artistRepository.save(track.getArtist());
            Album album = track.getAlbum();
            album.setArtist(savedArtist);
            track.setAlbum(albumRepository.save(album));
            track.setArtist(savedArtist);
            track.setGenre(genre);
            trackRepository.save(track);
        });
    }
}
