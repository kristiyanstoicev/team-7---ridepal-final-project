package com.example.ridepal.tasks;

import com.example.ridepal.exceptions.InvalidOperationException;
import com.example.ridepal.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

@Component
public class StartPopulationThreads extends Thread {

    private static final String[] GENRE_LIST = {"Classical", "Metal", "Pop", "Electro", "Rap"};
    private static final int WANTED_NUMBER_OF_TRACKS = 1200;
    private String tracksWithGenreToUpdate;

    private GenreRepository genreRepository;
    private TrackRepository trackRepository;
    private ArtistRepository artistRepository;
    private AlbumRepository albumRepository;
    private StopWatch stopWatch;

    @Autowired
    public StartPopulationThreads(GenreRepository genreRepository, TrackRepository trackRepository,
                                  ArtistRepository artistRepository, AlbumRepository albumRepository) {
        this(genreRepository, trackRepository, artistRepository, albumRepository, "All");
    }

    public StartPopulationThreads(GenreRepository genreRepository, TrackRepository trackRepository,
                                  ArtistRepository artistRepository, AlbumRepository albumRepository, String genre) {
        this.genreRepository = genreRepository;
        this.trackRepository = trackRepository;
        this.artistRepository = artistRepository;
        this.albumRepository = albumRepository;
        this.tracksWithGenreToUpdate = genre;
        stopWatch = new StopWatch();
    }

    @Override
    public void run() {
        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
        stopWatch.start();

        if ("All".equals(tracksWithGenreToUpdate)) {
            for (String genre : GENRE_LIST) {
                new UpdateDBFromDeezer(genreRepository, trackRepository, artistRepository,
                        albumRepository, genre, WANTED_NUMBER_OF_TRACKS);
            }
        } else {
            if (Arrays.stream(GENRE_LIST).noneMatch(genre -> genre.equals(tracksWithGenreToUpdate))) {
                throw new InvalidOperationException(tracksWithGenreToUpdate, this.getName());
            }
            new UpdateDBFromDeezer(genreRepository, trackRepository, artistRepository,
                    albumRepository, tracksWithGenreToUpdate, WANTED_NUMBER_OF_TRACKS);
        }

        stopWatch.stop();
        System.out.println("Time to populate DB (Genre: " + tracksWithGenreToUpdate + "): " + stopWatch.getTotalTimeSeconds() / 60 + " Minutes or " + stopWatch.getTotalTimeSeconds() + " Seconds.");
        System.out.println("Threads: " + threadSet.size());
    }
}
