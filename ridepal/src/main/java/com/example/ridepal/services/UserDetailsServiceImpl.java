package com.example.ridepal.services;

import com.example.ridepal.entities.Authority;
import com.example.ridepal.entities.User;
import com.example.ridepal.entities.UserDetail;
import com.example.ridepal.exceptions.EntityNotFoundException;
import com.example.ridepal.exceptions.InvalidOperationException;
import com.example.ridepal.repositories.AuthorityRepository;
import com.example.ridepal.repositories.UserDetailsRepository;
import com.example.ridepal.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    public static final int ONLY_USER_ROLE = 1;
    private UserDetailsRepository userDetailsRepository;
    private AuthorityRepository authorityRepository;
    private UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(UserDetailsRepository repository, AuthorityRepository authorityRepository,
                                  UserRepository userRepository) {
        this.userDetailsRepository = repository;
        this.authorityRepository = authorityRepository;
        this.userRepository=userRepository;
    }

    @Override
    public void createUserDetail(UserDetail userDetail) {

        log.info("createUserDetail is called. createdUser: {}", userDetail.getEmail());

        userDetailsRepository.save(userDetail);
    }

    @Override
    public UserDetail updateUserDetail(UserDetail userDetail, Principal principal) {

        log.info("updateUserDetail is called. loggedUser: {}, updatedUserDetail: {}", userDetail.getEmail(), principal.getName());

        List<Authority> authorityOfPrincipal = authorityRepository.findAllByUsername(principal.getName());

        if (hasAuthority(userDetail, principal, authorityOfPrincipal.size() == 1)) {
            throw new InvalidOperationException(principal.getName());
        }

        return userDetailsRepository.save(userDetail);
    }


    @Override
    public void softDeleteUserDetail(UserDetail userDetail, Principal principal) {

        log.info("deleteUserDetail is called. loggedUser: {}, deletedUserDetail: {}", userDetail.getEmail(), principal.getName());

        List<Authority> authorityOfPrincipal = authorityRepository.findAllByUsername(principal.getName());

        if (hasAuthority(userDetail, principal, authorityOfPrincipal.size() == ONLY_USER_ROLE)) {
            throw new InvalidOperationException(principal.getName());
        }

        User userInSecurity = userDetail.getUser();
        userInSecurity.setEnabled(false);
        userRepository.save(userInSecurity);
    }

    @Override
    public UserDetail findById(int id) {
        UserDetail userDetail = userDetailsRepository.findByIdAndUserEnabled(id,true);
        if (userDetail == null) {
            throw new EntityNotFoundException("User", id);
        }

        return userDetail;
    }

    @Override
    public UserDetail findByEmail(String email) {

        UserDetail userDetail = userDetailsRepository.findByEmailAndUserEnabled(email,true);

        if (userDetail == null) {
            throw new EntityNotFoundException("User", "email", email);
        }
        return userDetailsRepository.findByEmailAndUserEnabled(email,true);
    }

    @Override
    public boolean isUserExist(String email) {
        return userDetailsRepository.existsByEmail(email);
    }

    @Override
    public List<UserDetail> findAll() {
        return userDetailsRepository.findAllByUserEnabled(true);
    }


    private boolean hasAuthority(UserDetail userDetail, Principal principal, boolean b) {
        return !userDetail.getEmail().equals(principal.getName()) && b;
    }


}
