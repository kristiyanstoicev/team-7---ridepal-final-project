package com.example.ridepal.services;

import com.example.ridepal.entities.Genre;
import com.example.ridepal.entities.Playlist;
import com.example.ridepal.entities.Track;
import com.example.ridepal.entities.UserDetail;
import com.example.ridepal.models.GenreDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.security.Principal;
import java.util.List;
import java.util.Map;

public interface PlaylistService {

    List<Playlist> findAll();

    Page<Playlist> findAll(int page, int size);

    Page<Playlist> findPlaylistByTitleContaining(String title, int page, int size, int durationFrom, int durationTo, String sort);

    Playlist createPlaylist(int duration, List<GenreDTO> genres, Playlist playlist);

    void deletePlaylist(Playlist playlist, Principal principal);

    void editPlaylist(Playlist playlist, Principal principal);

    Playlist findById(int id);

    List<Playlist> findAllByCreator(int id);

    Page<Playlist> findAllByEnabledIsTrueOrderByRankAsc(int page, int size);

}
