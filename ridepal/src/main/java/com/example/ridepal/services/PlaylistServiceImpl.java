package com.example.ridepal.services;

import com.example.ridepal.entities.Artist;
import com.example.ridepal.entities.Authority;
import com.example.ridepal.entities.Playlist;
import com.example.ridepal.entities.Track;
import com.example.ridepal.exceptions.DuplicateEntityException;
import com.example.ridepal.exceptions.EntityNotFoundException;
import com.example.ridepal.exceptions.InvalidOperationException;
import com.example.ridepal.models.GenreDTO;
import com.example.ridepal.repositories.AuthorityRepository;
import com.example.ridepal.repositories.PlaylistRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class PlaylistServiceImpl implements PlaylistService {
    private static final int AVERAGE_DURATION_PER_SONG = 180;
    private static final int ALLOWED_SECONDS_TO_MISMATCH = 300;
    private static final String MESSAGE_DUPLICATED_ENTITY = "Playlist with this title already exist in your playlists";
    public static final int SIZE_OF_ROLES_WHEN_IS_NOT_ADMIN = 1;

    private PlaylistRepository playlistRepository;
    private TrackService trackService;
    private UserDetailsService userDetailsService;
    private AuthorityRepository authorityRepository;

    @Autowired
    public PlaylistServiceImpl(PlaylistRepository playlistRepository, TrackService trackService,
                               UserDetailsService userDetailsService, AuthorityRepository authorityRepository) {
        this.playlistRepository = playlistRepository;
        this.trackService = trackService;
        this.userDetailsService = userDetailsService;
        this.authorityRepository = authorityRepository;
    }

    @Override
    public List<Playlist> findAll() {
        return playlistRepository.findAllByEnabledIsTrue();
    }


    @Override
    public List<Playlist> findAllByCreator(int id) {
        return playlistRepository.findAllByCreatorIdAndEnabledIsTrue(id);
    }


    @Override
    public Page<Playlist> findAll(int page, int size) {

        return playlistRepository.findAllByEnabledIsTrueOrderByDateCreatedDesc(PageRequest.of(page, size));
    }


    @Override
    public Page<Playlist> findAllByEnabledIsTrueOrderByRankAsc(int page, int size) {
        return playlistRepository.findAllByEnabledIsTrueOrderByRankAsc(PageRequest.of(page, size));
    }

    @Override
    public Page<Playlist> findPlaylistByTitleContaining(String title, int page, int size, int durationFrom, int durationTo, String sort) {
        Pageable pageable;
        switch (sort) {
            case "asc":
                pageable = PageRequest.of(page, size, Sort.by("title").ascending());
                break;
            case "desc":
                pageable = PageRequest.of(page, size, Sort.by("title").descending());
                break;
            default:
                pageable = PageRequest.of(page, size);
        }
        return playlistRepository.findPlaylistByTitleContainingAndDurationBetweenAndEnabledIsTrue(title, durationFrom, durationTo, pageable);
    }

    public Playlist createPlaylist(int durationTravel, List<GenreDTO> genresDTO,
                                   Playlist playlist) {


        log.info("createPlaylist is called. creatorName: {}, playlistName: {}", playlist.getCreator().getEmail(), playlist.getTitle());

        generatePlaylist(durationTravel, genresDTO, playlist);

        return playlistRepository.save(playlist);
    }

    @Override
    public void deletePlaylist(Playlist playlist, Principal principal) {

        log.info("deletePlaylist is called. LoggedUser: {}, playlistName: {}", playlist.getCreator().getEmail(), playlist.getTitle());

        List<Authority> authorityOfPrincipal = authorityRepository.findAllByUsername(principal.getName());

        if (hasAuthority(playlist, principal, authorityOfPrincipal)) {
            throw new InvalidOperationException(principal.getName());
        }
        playlist.setEnabled(false);
        playlistRepository.save(playlist);
    }

    @Override
    public void editPlaylist(Playlist playlist, Principal principal) {

        log.info("editPlaylist is called. LoggedUser: {}, playlistName: {}", playlist.getCreator().getEmail(), playlist.getTitle());

        List<Authority> authorityOfPrincipal = authorityRepository.findAllByUsername(principal.getName());

        if (hasAuthority(playlist, principal, authorityOfPrincipal)) {
            throw new InvalidOperationException(principal.getName());
        }

        if (playlistRepository.findByTitleAndCreatorAndEnabledIsTrue
                (playlist.getTitle(), playlist.getCreator()) != null) {
            throw new DuplicateEntityException(MESSAGE_DUPLICATED_ENTITY);
        }
        playlistRepository.save(playlist);
    }


    @Override
    public Playlist findById(int id) {
        Playlist playlist= playlistRepository.findByIdAndEnabledIsTrue(id);

        if(playlist==null){
            throw new EntityNotFoundException("Playlist", id);
        }
        return playlist;
    }


    private void generatePlaylist(int durationTravel, List<GenreDTO> genres,
                                  Playlist playlist) {
        Set<Track> resultOfTracks = new HashSet<>();
        Set<Artist> artists = new HashSet<>();

        genres.forEach(genre -> {

            double durationPerGenre = durationTravel * (Double.valueOf(genre.getPercent()) / 100);

            int averageNumberOfTracks = (int) Math.floor(durationPerGenre / AVERAGE_DURATION_PER_SONG);

            List<Track> trackList;

            if (playlist.isTopTracksEnabled()) {
                trackList = trackService.getAllOrderedTracksByGenre(genre.getName());

            } else {
                trackList = trackService.findSpecificAmountOfTracksByGenre(genre.getName(), averageNumberOfTracks);
            }

            while (durationPerGenre > AVERAGE_DURATION_PER_SONG) {

                if (trackList.size() == 0) {
                    trackList = trackService.findSpecificAmountOfTracksByGenre(genre.getName(), averageNumberOfTracks);
                }

                int indexForTrackList = trackList.size() - 1;
                Track randomTrack = trackList.get(indexForTrackList);

                if(randomTrack.getPreview().isEmpty()){
                    trackList.remove(randomTrack);
                    continue;
                }

                if (!playlist.isArtistRepeated()) {
                    if (artists.contains(randomTrack.getArtist())) {
                        trackList.remove(randomTrack);
                        continue;
                    }
                }
                durationPerGenre -= randomTrack.getDuration();

                if (durationPerGenre < -AVERAGE_DURATION_PER_SONG) {
                    break;
                }

                if (resultOfTracks.add(randomTrack)) {
                    playlist.setDuration(playlist.getDuration() + randomTrack.getDuration());
                    playlist.setRank(playlist.getRank() + randomTrack.getRank());
                    trackList.remove(randomTrack);
                    artists.add(randomTrack.getArtist());
                } else {
                    trackList.remove(randomTrack);
                    durationPerGenre += randomTrack.getDuration();
                }
            }
        });

        playlist.setRank(playlist.getRank() / resultOfTracks.size());
        playlist.getTracks().addAll(resultOfTracks);

    }

    private boolean hasAuthority(Playlist playlist, Principal principal, List<Authority> authorityOfPrincipal) {
        return !playlist.getCreator().getEmail().equals(principal.getName())
                && authorityOfPrincipal.size() == SIZE_OF_ROLES_WHEN_IS_NOT_ADMIN;
    }
}
