package com.example.ridepal.models;

import com.example.ridepal.entities.common.GenderType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor// (access = AccessLevel.PRIVATE)
public class UserDto {

    @Size(min = 2,
            max =30,
            message = "Name size should not be less than {min} and more than {max} characters long")
    private String firstName;

    @Size(min = 2,
            max=30,
            message = "Name size should not be less than {min} and more than {max} characters long")
    private String lastName;

    @Email(message = "Email should be valid")
    private String email;

    @Size(min = 6,
            max =30,
            message = "Password size should not be less than {min} and more than {max} characters long")
    private String password;

    @Size(min = 6,
            max =30,
            message = "Password size should not be less than {min} and more than {max} characters long")
    private String passwordConfirmation;

    private String picture;

    @NotNull
    @Min(5)
    @Max(120)
    private Integer age;


    private GenderType gender;

}
