package com.example.ridepal.helper;

import com.example.ridepal.models.PlaylistDto;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
@Component
public class ControllerHelper {

    private String bingMapKey;

    @Autowired
    public ControllerHelper(Environment env) {
        this.bingMapKey =env.getProperty("bingMap.key");
    }

    public int getDuration(PlaylistDto playlistDto) {

        String bingMapDistanceMatrixUrl = String
                .format("http://dev.virtualearth.net/REST/v1/Routes/driving?wayPoint.1=%s&waypoint.2=%s&distanceUnit=km&key=%s",
                        playlistDto.getStart(), playlistDto.getFinish(), bingMapKey);

        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(bingMapDistanceMatrixUrl, String.class);
        JSONObject jsonResponse = new JSONObject(response);
        JSONObject duration = jsonResponse
                .getJSONArray("resourceSets")
                .getJSONObject(0)
                .getJSONArray("resources")
                .getJSONObject(0)
                .getJSONArray("routeLegs")
                .getJSONObject(0);
        return duration.getInt("travelDuration");
    }
}
