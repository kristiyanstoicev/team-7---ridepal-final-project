package com.example.ridepal.controllers;

import com.example.ridepal.entities.Playlist;
import com.example.ridepal.entities.UserDetail;
import com.example.ridepal.helper.ControllerHelper;
import com.example.ridepal.models.DtoMapper;
import com.example.ridepal.models.GenreDTO;
import com.example.ridepal.models.PlaylistDto;
import com.example.ridepal.models.UpdatePlaylistDTO;
import com.example.ridepal.services.GenreService;
import com.example.ridepal.services.PlaylistService;
import com.example.ridepal.services.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Controller
public class PlaylistController {

    private static final int ALLOWED_SECONDS_FOR_PLAYLIST = 600;
    private static final String INVALID_LOCATION_ERROR_MESSAGE = "There is no coverage for this imagery type at the requested location.";
    private static final String ERROR_MAP_DESTINATION_NOT_COMPLETE = "Error: map destination not complete...";
    private static final String BING_MAP_REQUEST_URL = "http://dev.virtualearth.net/REST/v1/Imagery/Map/Road/Routes?wp.0=%s&wp.1=%s&distanceUnit=km&key=%s";
    private PlaylistService playlistService;
    private GenreService genreService;
    private UserDetailsService userDetailService;
    private String bingMapKey;
    private DtoMapper dtoMapper;
    private ControllerHelper controllerHelper;


    @Autowired
    public PlaylistController(PlaylistService playlistService, GenreService genreService,
                              UserDetailsService userDetailService, Environment env,
                              DtoMapper dtoMapper, ControllerHelper controllerHelper) {
        this.playlistService = playlistService;
        this.genreService = genreService;
        this.userDetailService = userDetailService;
        this.dtoMapper = dtoMapper;
        this.controllerHelper = controllerHelper;
        bingMapKey = env.getProperty("bingMap.key");
    }

    @GetMapping("/generate-playlist")
    public String generatePlaylist(Model model, Principal principal) {
        UserDetail loggedUser;

        if (principal != null) {
            loggedUser = userDetailService.findByEmail(principal.getName());
        } else {
            loggedUser = new UserDetail();
        }

        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("playlistDto", new PlaylistDto());
        model.addAttribute("key", bingMapKey);
        model.addAttribute("genres", genreService.findAll());

        return "generate-playlist";
    }

    @PostMapping("/generate-playlist")
    public String generatePlaylist(@ModelAttribute PlaylistDto playlistDto, Principal principal) {

        int duration = controllerHelper.getDuration(playlistDto);

        if (duration < ALLOWED_SECONDS_FOR_PLAYLIST) {
            return "too short destination";
        }

        List<GenreDTO> favouriteGenresDTO = new ArrayList<>();

        Playlist playlist = dtoMapper.createFromDto(playlistDto, favouriteGenresDTO, principal);

        int id = playlistService.createPlaylist(duration, favouriteGenresDTO, playlist).getId();

        return String.format("redirect:/playlist/%d", id);
    }

    @RequestMapping(value = {"/destinationMap"}, method = RequestMethod.GET)
    public ResponseEntity<String> getMap(@RequestParam(name = "first") String first, @RequestParam(name = "second") String second) {

        String bingMapDistanceMatrixUrl = String
                .format(BING_MAP_REQUEST_URL,
                        first, second, bingMapKey);

        RestTemplate restTemplate = new RestTemplate();

        byte[] imageBytes = new byte[0];
        try {
            imageBytes = restTemplate.getForObject(bingMapDistanceMatrixUrl, byte[].class);
        } catch (RestClientException e) {
            System.out.println(ERROR_MAP_DESTINATION_NOT_COMPLETE);
            return new ResponseEntity<>(INVALID_LOCATION_ERROR_MESSAGE,
                    HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity<>(Base64.getEncoder().encodeToString(imageBytes), HttpStatus.OK);

    }

    @GetMapping("/playlist/{id}")
    public String showPlaylist(@PathVariable int id, Model model, Principal principal) {
        if (principal != null) {
            model.addAttribute("loggedUser", userDetailService.findByEmail(principal.getName()));
        }
        Playlist playlist = playlistService.findById(id);
        model.addAttribute("playlist", playlist);
        model.addAttribute("userPlaylists", playlistService.findAllByCreator(playlist.getCreator().getId()));
        return "single-playlist";

    }

    @PostMapping("/playlist/{id}/update")
    @ResponseBody
    public ResponseEntity<Playlist> updatePlaylist(@PathVariable int id, @RequestBody UpdatePlaylistDTO updatePlaylistDTO,
                                                   Principal principal) {
        Playlist playlistToEdit = playlistService.findById(id);
        playlistService.editPlaylist(dtoMapper.updatePlaylistTitleFromDTO(updatePlaylistDTO, playlistToEdit), principal);
        return new ResponseEntity<>(playlistToEdit, HttpStatus.OK);
    }

    @GetMapping("/playlist/{id}/delete")
    public String deletePlaylist(@PathVariable int id, Principal principal) {
        Playlist playlistToDelete = playlistService.findById(id);
        playlistService.deletePlaylist(playlistToDelete, principal);
        return "redirect:/";
    }

    @GetMapping("/playlist/{id}/songs")
    public ResponseEntity<Playlist> getPlaylist(@PathVariable int id) {
        return new ResponseEntity<>(playlistService.findById(id), HttpStatus.OK);
    }
}
