package com.example.ridepal.controllers.REST.deezerAPI.tracksDTOs;

import com.example.ridepal.entities.Track;
import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TrackDTO {
    @JsonProperty("data")
    public List<Track> data = null;
    @JsonProperty("next")
    public String next;
}