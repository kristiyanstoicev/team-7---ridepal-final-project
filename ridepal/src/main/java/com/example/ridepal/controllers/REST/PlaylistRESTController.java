package com.example.ridepal.controllers.REST;

import com.example.ridepal.entities.Playlist;
import com.example.ridepal.helper.ControllerHelper;
import com.example.ridepal.models.DtoMapper;
import com.example.ridepal.models.GenreDTO;
import com.example.ridepal.models.PlaylistDto;
import com.example.ridepal.services.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/playlists")
public class PlaylistRESTController {

    private PlaylistService playlistService;
    private DtoMapper dtoMapper;
    private ControllerHelper controllerHelper;

    @Autowired
    public PlaylistRESTController(PlaylistService playlistService, DtoMapper dtoMapper, ControllerHelper controllerHelper) {
        this.playlistService = playlistService;
        this.dtoMapper = dtoMapper;
        this.controllerHelper = controllerHelper;
    }

    @GetMapping("/")
    public List<Playlist> getAll() {
        return playlistService.findAll();
    }

    @GetMapping("/{id}")
    public  ResponseEntity<Playlist> getById(@PathVariable int id) {

        Playlist playlist = playlistService.findById(id);

         return ResponseEntity.status(HttpStatus.OK).body(playlist);
    }


    @PostMapping("/")
    public ResponseEntity<?> generatePlaylist(PlaylistDto playlistDto, Principal principal) {

        int duration = controllerHelper.getDuration(playlistDto);

        List<GenreDTO> favouriteGenresDTO = new ArrayList<>();

        Playlist playlist = dtoMapper.createFromDto(playlistDto, favouriteGenresDTO, principal);

        playlistService.createPlaylist(duration, favouriteGenresDTO, playlist);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id]")
    public ResponseEntity<?> updatePlaylist(@PathVariable int id,
                                            PlaylistDto playlistDto, Principal principal){

        Playlist playlist = playlistService.findById(id);
        playlist.setTitle(playlistDto.getTitle());
        playlistService.editPlaylist(playlist,principal);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePlaylist(@PathVariable int id, Principal principal){

        Playlist playlist = playlistService.findById(id);
        playlistService.deletePlaylist(playlist,principal);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
