package com.example.ridepal.controllers.REST.deezerAPI.searchDTOs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Playlist {

    @JsonProperty("id")
    public String id;
    @JsonProperty("nb_tracks")
    public Integer nbTracks;
    @JsonProperty("tracklist")
    public String tracklist;

}
