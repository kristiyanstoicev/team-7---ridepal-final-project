package com.example.ridepal.exceptions.handlers;


import com.example.ridepal.controllers.HomeController;
import com.example.ridepal.controllers.PlaylistController;
import com.example.ridepal.controllers.UserController;
import com.example.ridepal.exceptions.DuplicateEntityException;
import com.example.ridepal.exceptions.EntityNotFoundException;
import com.example.ridepal.exceptions.InvalidOperationException;
import com.example.ridepal.helper.ControllerHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

@ControllerAdvice(basePackageClasses = {PlaylistController.class, UserController.class, HomeController.class,
        ControllerHelper.class})
@Order(1)
@Slf4j
public class GlobalViewHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    public String handleEntityNotFound(EntityNotFoundException ex, Model model) {

        log.error(" handleEntityNotFound is called. Message: {}, Status: {}", ex.getMessage(), HttpStatus.NOT_FOUND.value());

        model.addAttribute("errorMessage", ex.getMessage());
        model.addAttribute("errorStatus", HttpStatus.NOT_FOUND.value());

        return "error";
    }

    @ExceptionHandler(DuplicateEntityException.class)
    public String handleDuplicateEntity(DuplicateEntityException ex, Model model) {

        log.error(" handleEntityNotFound is called. Message: {}, Status: {}", ex.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY.value());

        model.addAttribute("errorMessage", ex.getMessage());
        model.addAttribute("errorStatus", HttpStatus.UNPROCESSABLE_ENTITY.value());

        return "error";

    }

    @ExceptionHandler(InvalidOperationException.class)
    public String handleDuplicateEntity(InvalidOperationException ex, Model model) {

        log.error(" handleEntityNotFound is called. Message: {}, Status: {}", ex.getMessage(), HttpStatus.FORBIDDEN.value());

        model.addAttribute("errorMessage", ex.getMessage());
        model.addAttribute("errorStatus", HttpStatus.FORBIDDEN.value());

        return "error";

    }

    @ExceptionHandler(HttpClientErrorException.class)
    public String handleHttpClientError(InvalidOperationException ex, Model model) {

        log.error(" handleHttpClientError is called. Message: {}, Status: {}", ex.getMessage(), HttpStatus.NOT_FOUND.value());

        model.addAttribute("errorMessage", ex.getMessage());
        model.addAttribute("errorStatus", HttpStatus.FORBIDDEN.value());

        return "error";

    }
}