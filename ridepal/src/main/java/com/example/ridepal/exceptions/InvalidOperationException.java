package com.example.ridepal.exceptions;

public class InvalidOperationException extends RuntimeException {


    public InvalidOperationException(String email) {
        super(String.format("User with email %s does not have access for this operation", email));
    }

    public InvalidOperationException(int seconds) {
        super(String.format("Playlist with duration less than %d is not allowrd", seconds));
    }

    public InvalidOperationException(String genre, String thread) {
        super(String.format("%s: Genre %s does not exists.", thread, genre));
    }

}
