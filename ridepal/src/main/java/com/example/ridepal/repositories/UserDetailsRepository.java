package com.example.ridepal.repositories;

import com.example.ridepal.entities.UserDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDetailsRepository extends JpaRepository<UserDetail, Integer> {

    UserDetail findByEmailAndUserEnabled(String email, boolean status);

    boolean existsByEmail(String email);

    List<UserDetail> findAllByUserEnabled(boolean status);

    UserDetail findByIdAndUserEnabled(int id, boolean status);
}
