package com.example.ridepal.repositories;

import com.example.ridepal.entities.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenreRepository extends JpaRepository<Genre, String> {
    Genre findByName(String name);
}
