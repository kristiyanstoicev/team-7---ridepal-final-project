package com.example.ridepal.repositories;

import com.example.ridepal.entities.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlbumRepository extends JpaRepository<Album, String> {
    Album getByTitle(String title);
}
