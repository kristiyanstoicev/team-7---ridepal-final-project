package com.example.ridepal.services;

import com.example.ridepal.entities.Authority;
import com.example.ridepal.entities.User;
import com.example.ridepal.entities.UserDetail;
import com.example.ridepal.exceptions.EntityNotFoundException;
import com.example.ridepal.exceptions.InvalidOperationException;
import com.example.ridepal.repositories.AuthorityRepository;
import com.example.ridepal.repositories.UserDetailsRepository;
import com.example.ridepal.repositories.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;


@RunWith(MockitoJUnitRunner.Silent.class)
public class UserDetailsServicesImplTests {

    private UserDetail testUserDetail = new UserDetail();
    private User userFromSecurity = new User();
    private final int testId = 1;

    @Mock
    UserDetailsRepository userDetailsRepository;

    @Mock
    UserRepository userRepository;

    @Mock
    AuthorityRepository authorityRepository;

    @Mock
    private Principal principal;


    @InjectMocks
    UserDetailsServiceImpl service;

    @Test
    public void deleteUserShould_DeleteUser_whenUserDeleteHimself() {

        //Arrange
        String email = "test@abv.bg";
        testUserDetail.setEmail(email);
        testUserDetail.setUser(userFromSecurity);


        List<Authority> authorities = new ArrayList<>();
        authorities.add(new Authority("test@abv.bg", "ROLE_USER"));

        Mockito.when(authorityRepository.findAllByUsername(anyString())).thenReturn(authorities);
        Mockito.when(principal.getName()).thenReturn(email);

        //Act
        service.softDeleteUserDetail(testUserDetail, principal);
        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).save(testUserDetail.getUser());
    }

    @Test
    public void deleteUserShould_DeleteUser_whenUserIsAdmin() {

        //Arrange
        String email = "test@abv.bg";
        testUserDetail.setEmail(email);
        testUserDetail.setUser(userFromSecurity);

        List<Authority> authorities = new ArrayList<>();
        authorities.add(new Authority("test@abv.bg", "ROLE_USER"));
        authorities.add(new Authority("test@abv.bg", "ROLE_ADMIN"));

        Mockito.when(authorityRepository.findAllByUsername(anyString())).thenReturn(authorities);
        Mockito.when(principal.getName()).thenReturn("other@email.bg");

        //Act
        service.softDeleteUserDetail(testUserDetail, principal);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).save(testUserDetail.getUser());
    }

    @Test(expected = InvalidOperationException.class)
    public void deleteUserShould_throwException_whenUserNotAdminNorPrincipal() {

        //Arrange
        String email = "test@abv.bg";
        testUserDetail.setEmail(email);
        testUserDetail.setUser(userFromSecurity);

        List<Authority> authorities = new ArrayList<>();
        authorities.add(new Authority("test@abv.bg", "ROLE_USER"));


        Mockito.when(authorityRepository.findAllByUsername(anyString())).thenReturn(authorities);
        Mockito.when(principal.getName()).thenReturn("other@email.bg");

        //Act
        service.softDeleteUserDetail(testUserDetail, principal);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).save(testUserDetail.getUser());
    }

    @Test(expected = InvalidOperationException.class)
    public void editUserShould_throwException_whenUserNotAdminNorPrincipal() {

        //Arrange
        String email = "test@abv.bg";
        testUserDetail.setEmail(email);
        testUserDetail.setUser(userFromSecurity);

        List<Authority> authorities = new ArrayList<>();
        authorities.add(new Authority("test@abv.bg", "ROLE_USER"));


        Mockito.when(authorityRepository.findAllByUsername(anyString())).thenReturn(authorities);
        Mockito.when(principal.getName()).thenReturn("other@email.bg");

        //Act
        service.updateUserDetail(testUserDetail, principal);

        //Assert
        Mockito.verify(userDetailsRepository, Mockito.times(1)).save(testUserDetail);
    }

    @Test
    public void editUserShould_editUser_whenUserIsAdmin() {

        //Arrange
        String email = "test@abv.bg";
        testUserDetail.setEmail(email);
        testUserDetail.setUser(userFromSecurity);

        List<Authority> authorities = new ArrayList<>();
        authorities.add(new Authority("test@abv.bg", "ROLE_USER"));
        authorities.add(new Authority("test@abv.bg", "ROLE_ADMIN"));


        Mockito.when(authorityRepository.findAllByUsername(anyString())).thenReturn(authorities);
        Mockito.when(principal.getName()).thenReturn("other@email.bg");

        //Act
        service.updateUserDetail(testUserDetail, principal);

        //Assert
        Mockito.verify(userDetailsRepository, Mockito.times(1)).save(testUserDetail);
    }

    @Test
    public void editUserShould_editUser_whenUserIsPrincipal() {

        //Arrange
        String email = "test@abv.bg";
        testUserDetail.setEmail(email);
        testUserDetail.setUser(userFromSecurity);

        List<Authority> authorities = new ArrayList<>();
        authorities.add(new Authority("test@abv.bg", "ROLE_USER"));


        Mockito.when(authorityRepository.findAllByUsername(anyString())).thenReturn(authorities);
        Mockito.when(principal.getName()).thenReturn(email);

        //Act
        service.updateUserDetail(testUserDetail, principal);

        //Assert
        Mockito.verify(userDetailsRepository, Mockito.times(1)).save(testUserDetail);
    }


    @Test
    public void createUserShould_callUser_WhenUserNotExists() {
        //Act
        service.createUserDetail(testUserDetail);
        //Assert
        Mockito.verify(userDetailsRepository, Mockito.times(1)).save(testUserDetail);
    }


    @Test
    public void findByIdShould_throwError_WhenUserNotExists() {
        //Act Arrange

        Mockito.when(userDetailsRepository.findByIdAndUserEnabled(testId,false)).thenThrow(EntityNotFoundException.class);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> service.findById(testId));
    }

    @Test
    public void findByIdShould_returnUser_WhenUserExists() {
        //Arrange

        UserDetail expectedUser = new UserDetail();

        //Act
        Mockito.when(userDetailsRepository.findByIdAndUserEnabled(testId,true)).thenReturn(expectedUser);
        UserDetail returnedUser = service.findById(testId);

        //Assert
        Assert.assertSame(expectedUser, returnedUser);

    }

    @Test
    public void findByEmailShould_throwError_WhenUserNotExists() {
        //Act Arrange

        Mockito.when(userDetailsRepository.findByEmailAndUserEnabled("test@email.bg",false))
                .thenThrow(EntityNotFoundException.class);

        //Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> service.findByEmail("test@email.bg"));
    }

    @Test
    public void findByEmailShould_returnUser_WhenUserExists() {
        //Arrange

        UserDetail expectedUser = new UserDetail();

        //Act
        Mockito.when(userDetailsRepository.findByEmailAndUserEnabled("test@abv.bg",true)).thenReturn(expectedUser);
        UserDetail returnedUser = service.findByEmail("test@abv.bg");

        //Assert
        Assert.assertSame(expectedUser, returnedUser);

    }

    @Test
    public void getAllUsers_Should_callAllUsers() {
        // Arrange
        Mockito.when(userDetailsRepository.findAllByUserEnabled(true)).thenReturn(Arrays.asList(
                new UserDetail(),
                new UserDetail()
        ));

        // Act
        List<UserDetail> result = service.findAll();

        // Assert
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void isUserExist_Should_callExistsByEmailMethod() {
        // Arrange

        Mockito.when(userDetailsRepository.existsByEmail("test@abv.bg")).thenReturn(true);

        // Act
        Boolean result = service.isUserExist("test@abv.bg");

        // Assert
        Assert.assertEquals(true,result);
    }
}
