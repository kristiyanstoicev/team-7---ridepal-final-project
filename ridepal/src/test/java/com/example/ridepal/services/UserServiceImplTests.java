package com.example.ridepal.services;


import com.example.ridepal.entities.User;
import com.example.ridepal.repositories.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceImplTests {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void getByUserName_Should_callRepository() {

        //Arrange
        User expectedUser = new User();
        Mockito.when(userRepository.getByUsername("test")).thenReturn(expectedUser);

        //Act
        User returnedUser = userService.getByUsername("test");



        Assert.assertEquals(expectedUser,returnedUser);
    }

}
