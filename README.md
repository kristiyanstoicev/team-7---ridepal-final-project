![alt text](logo_with_name_transparent.png "logo")

# RidePal 
A Spring web application which allows generating playlist of tracks with user's
favourite genres and the duration of the playlist should be the same as the 
user's trip. The trip duration is calculated through external service of Bing map.

## Team members

* Dimitar Kurtev - [(https://gitlab.com/dimitar.kurtev95)](url)
* Kristiyan Stoitsev - [(https://gitlab.com/kristiyanstoicev)](url)


## Project Description

#### Public part   

The public part consists of a home page displaying top 10 playlist ordered by their rank.
It includes login and register pop up forms as well as contact form.

![alt text](homepage.png)


#### Private part

Users see everything visible to website visitors and they have authority to generate new playlists.

![alt text](generate-playlist.png)

Logged users can visit the single page of playlist and listen to it tracks

![alt text](single-playlist.png)

They can visit and edit their personal user-profile page as well as another user-profiles.
 
![alt text](user-profile.png)

Modal box for editing personal details with js validations.

![alt text](edit-user.png)

#### Administrative part

Admin can see all existing users on special list in his personal profile page and 
has access to edit and delete any user profile or playlist. 

![alt text](admin_panel.png)

## Technologies
* Java 
* Spring
* Spring Data JPA
* MySQL 
* Spring Security
* Thymeleaf
* HTML
* CSS
* JQuery
* AJAX

## Notes
* Over 80% Unit test code coverage of the business logic. Using Mockito for testing.
* Used DTO (data transfer objects) 
* REST Controllers 
* Used Logger
* Used Swagger

## Database Diagram
![alt text](ridepal.png "database")

## URL 
[(https://ridepal-team7.herokuapp.com/api/playlists/)](url)

## Trello
[(https://trello.com/b/0YJ9Q0dX/ridepal)]
